package com.dyllantan.azureeventhub;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONObject;

import java.net.URLEncoder;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class MainActivity extends AppCompatActivity {

    AsyncHttpClient client;

    String uri = "https://magnea-funnel-dev-m-ns.servicebus.windows.net/magnea-funnel-dev-m";
    String actualUrl = "https://magnea-funnel-dev-m-ns.servicebus.windows.net/magnea-funnel-dev-m/messages";
    String sasKeyValue = "IqbvAVBMV7gHW+37LJhTeJHi0HMzRWpR+jHJsKfM63Q=";
    String policyName = "SendRule";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView versionText = (TextView) findViewById(R.id.androidVersion);
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        versionText.setText("Version: " + pInfo.versionName);

        openConnection();
    }

    public void openConnection() {
        client = new AsyncHttpClient();

        String sas = generateSasToken();

        client.addHeader("Authorization", sas);
    }

    // Event Hub - Send Event
    public void sendEventClick(View v) {
        JSONObject params = new JSONObject();
        try {
            params.put("SensorId", 12);
            params.put("CoordinateX", 1);
            StringEntity entity = new StringEntity(params.toString());

            client.post(this, actualUrl, entity, "application/json", new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    Toast.makeText(getApplicationContext(), "Status: " + statusCode, Toast.LENGTH_LONG).show();
                    Log.d("Status:", Integer.toString(statusCode));
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Toast.makeText(getApplicationContext(), "Failed to send", Toast.LENGTH_LONG).show();
                }
            });
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private String generateSasToken() {

        try {
            String targetUri = URLEncoder.encode(uri.toString().toLowerCase(), "UTF-8").toLowerCase();

            long expiresOnDate = System.currentTimeMillis();
            int expiresInMins = 1; // 1 hour
            expiresOnDate += expiresInMins * 60 * 1000;
            long expires = expiresOnDate / 1000;
            String toSign = targetUri + "\n" + expires;

            // Get an hmac_sha1 key from the raw key bytes
            SecretKeySpec signingKey = new SecretKeySpec(sasKeyValue.getBytes(), "HmacSHA256");

            // Get an hmac_sha1 Mac instance and initialize with the signing key
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(signingKey);

            // Compute the hmac on input data bytes
            byte[] rawHmac = mac.doFinal(toSign.getBytes("UTF-8"));

            // Using android.util.Base64 for Android Studio instead of
            // Apache commons codec
            String signature = URLEncoder.encode(Base64.encodeToString(rawHmac, Base64.NO_WRAP).toString(), "UTF-8");

            // Construct authorization string
            String token = "SharedAccessSignature sr=" + targetUri + "&sig=" + signature + "&se=" + expires + "&skn=" + policyName;
            Toast.makeText(getApplicationContext(), token, Toast.LENGTH_LONG).show();
            Log.d("SAS: ", token);

            return token;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
